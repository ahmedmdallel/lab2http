package lab2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import httpd.HttpConnection;

public class Httpd {
	private Socket connect;
	public static int portNumber;
	public static String webroot;

	public static void serverConfiguration() throws IOException {
		File file = new File("./src/main/resources/conf.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String st;
		while ((st = br.readLine()) != null) {
			if (st.contains("Port")) {
				portNumber = Integer.parseInt(st.substring(st.indexOf("=") + 1, st.length()));
			}
			if (st.contains("WebRoot")) {
				webroot = st.substring(st.indexOf("=") + 1, st.length());
			}
		}
		Properties prop = new Properties();
		OutputStream output = null;
		output = new FileOutputStream("config.properties");
		prop.setProperty("Port",String.valueOf(portNumber));
	prop.setProperty("Content-type", "text/html");
		prop.store(output, null);
		FileInputStream input = new FileInputStream("config.properties");
	prop.load(input);

	}

	public static void requestLoop() throws IOException {
		ServerSocket serverConnect = new ServerSocket(portNumber);
	
		while (true) {
			HttpConnection myServer = new HttpConnection(serverConnect.accept());
			
			Thread thread = new Thread(myServer);
		thread.start();
		}
	}

	public static void main(String[] args) throws IOException {
		serverConfiguration();
		requestLoop();
	}
}
