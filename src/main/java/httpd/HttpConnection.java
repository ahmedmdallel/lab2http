package httpd;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.sound.midi.Soundbank;

public class HttpConnection implements Runnable {
	static final File WEB_ROOT = new File(".");
	static final String DEFAULT_FILE = "./src/main/resources/index.html";
	static final String FILE_NOT_FOUND = "./src/main/resources/Notfound.html";
	static final String BAD_REQUEST = "./src/main/resources/Badrequest.html";
	private Socket connect;

	public HttpConnection(Socket c) {
		connect = c;
	}

	public String analyseRequest(String method) {
		if (!method.equals("GET") && !method.equals("HEAD")) {
			return "400";
		}
		if (method.equals("GET")) {
			return "200";
		}
		return "401";

	}

	public void run() {
		BufferedReader in = null;
		PrintWriter out = null;
		BufferedOutputStream dataOut = null;
		String fileRequested = null;

		try {
			in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
			out = new PrintWriter(connect.getOutputStream());	
			dataOut = new BufferedOutputStream(connect.getOutputStream());
			String input = in.readLine();
			StringTokenizer parse = new StringTokenizer(input);
			String method = parse.nextToken().toUpperCase();
			fileRequested = parse.nextToken().toLowerCase();
			String req = analyseRequest(method);
			HttpLog log = new HttpLog("filelog.log");
			log.add(connect.getRemoteSocketAddress().toString(), input, Integer.valueOf(req));

			if (req.equals("400")) {

				File file = new File(WEB_ROOT, BAD_REQUEST);
				int fileLength = (int) file.length();

				String contentMimeType = "text/html";

				byte[] fileData = readFileData(file, fileLength);

				out.println("HTTP/1.1 400 Not Implemented");
				out.println("Date: " + new Date());
				out.println("Content-type: " + contentMimeType);
				out.println("Content-length: " + fileLength);
				out.println();
				out.flush();
				dataOut.write(fileData, 0, fileLength);
				dataOut.flush();

			} else {

				if (fileRequested.endsWith("/")) {
					fileRequested += DEFAULT_FILE;
				}

				File file = new File(WEB_ROOT, fileRequested);
			
				int fileLength = (int) file.length();
				String content = getContentType(fileRequested);

				if (req.equals("200")) {
					byte[] fileData = readFileData(file, fileLength);
					changeResourcesTypes(content);

					out.println("HTTP/1.1 200 OK");
					out.println("Date: " + new Date());
					out.println("Content-type: " + content);
					out.println("Content-length: " + fileLength);
					out.println(); 
					out.flush(); 
					
					dataOut.write(fileData, 0, fileLength);
					dataOut.flush();
				}

			}

		} catch (FileNotFoundException fnfe) {
			try {
				fileNotFound(out, dataOut, fileRequested);
			} catch (IOException ioe) {
				System.err.println("Error with file not found exception : " + ioe.getMessage());
			}

		} catch (IOException ioe) {
			System.err.println("Server error : " + ioe);
		} finally {
			try {
				in.close();
				out.close();
				dataOut.close();
				connect.close();
			} catch (Exception e) {
				System.err.println("Error closing stream : " + e.getMessage());
			}

		}
	}

	private byte[] readFileData(File file, int fileLength) throws IOException {
		FileInputStream fileIn = null;
		byte[] fileData = new byte[fileLength];

		try {
			fileIn = new FileInputStream(file);
			fileIn.read(fileData);
		} finally {
			if (fileIn != null)
				fileIn.close();
		}

		return fileData;
	}

	private String getContentType(String fileRequested) {
		if (fileRequested.endsWith(".htm") || fileRequested.endsWith(".html"))
			return "text/html";
		else
			return "text/plain";
	}

	private void fileNotFound(PrintWriter out, OutputStream dataOut, String fileRequested) throws IOException {
		File file = new File(WEB_ROOT, FILE_NOT_FOUND);
		int fileLength = (int) file.length();
		String content = "text/html";
		byte[] fileData = readFileData(file, fileLength);
		changeResourcesTypes(content);
		out.println("HTTP/1.1 404 File Not Found");
		out.println("Date: " + new Date());
		out.println("Content-type: " + content);
		out.println("Content-length: " + fileLength);
		out.println();
		out.flush();

		dataOut.write(fileData, 0, fileLength);
		dataOut.flush();

	}

	public void changeResourcesTypes(String content) throws IOException {
		File file = new File("./src/main/resources/resourceTypes.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String st;
		String type = "text/html";
		while ((st = br.readLine()) != null) {
			if (content.equals(st)) {
				type = st;
			}
		}
		Properties prop = new Properties();
		OutputStream output = null;
		output = new FileOutputStream("config.properties");
		prop.setProperty("Content-type", "text/html");
		prop.store(output, null);
		FileInputStream input = new FileInputStream("config.properties");
		prop.load(input);
	}

}
