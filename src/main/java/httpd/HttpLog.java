package httpd;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HttpLog {
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	PrintStream logs;
public HttpLog(String logFile) throws FileNotFoundException {
	this.logs=new PrintStream(new FileOutputStream(logFile));
}
synchronized String add (String address, String request, int status) {
	this.logs.print("["+address+"]  "+"["+dateFormat.format(date)+"]   "+
            "["+request+"]   "+"["+status+"]");
	return "["+address+"]  "+"["+dateFormat.format(date)+"]   "+
            "["+request+"]   "+"["+status+"]";
}
}
